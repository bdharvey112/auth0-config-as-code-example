#!/usr/bin/env bash
set -euo pipefail

sed -e "s|<AUTH0_CLIENT_ID>|$AUTH0_CLIENT_ID|" -e "s|<AUTH0_CLIENT_SECRET>|$AUTH0_CLIENT_SECRET|" config.json > /tmp/config.json

 a0deploy import --config_file config.prod.json --input_file ./config/
