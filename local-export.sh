#!/usr/bin/env bash
set -euo pipefail

sed -e "s|<AUTH0_CLIENT_ID>|$AUTH0_CLIENT_ID|" -e "s|<AUTH0_CLIENT_SECRET>|$AUTH0_CLIENT_SECRET|" config.json > /tmp/config.json

# Export the config from auth0 to the local directory
a0deploy export --config_file config.json --format directory --output_folder config
